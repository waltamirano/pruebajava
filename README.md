Para el supuesto de negocio de un banco en el cual los clientes poseen "n" cantidad de cuentas bancarias se diseño un api que provee de diversos mecanimos para el registro  y afectcion de saldos en las cuentas.


La api consta de 2 procesos principales
 el primero es la creacion del cliente
 el segundo es la creacion de la cuenta bancaria, sobre este ultimo se realizara los procesos de debitos y creditos.

 la API esta diseñada con Srping Boot y JPA, se despligue en servidor web Apache Tomcat 9

los metodos se encuentran descritos en la siguiente URL

 http://localhost:8080/banco-api/swagger-ui.html#/cuenta-controller

 para realizar pruebas en la API se puede utilizar la url antes descrita o la aplicacion postMan

 se diseño uba base de datos relacion en PostgreSql

