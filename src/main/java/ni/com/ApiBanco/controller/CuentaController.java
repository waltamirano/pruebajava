/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.ApiBanco.controller;

import java.util.List;
import ni.com.ApiBanco.model.CuentaCliente;
import ni.com.ApiBanco.model.CuentaMovimientos;
import ni.com.ApiBanco.service.CuentaService;
import ni.com.ApiBanco.service.MovimientosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author wiston.altamirano
 */
@CrossOrigin(origins = "http://localhost:4200",maxAge = 3600)
@RestController
@RequestMapping({"/cuenta"})
public class CuentaController {

    @Autowired
    CuentaService cuentaService;

    @Autowired
    MovimientosService movService;

    @GetMapping
    public List<CuentaCliente> listarCuenta() {
        return cuentaService.listar();
    }

    @PostMapping
    public CuentaCliente agregar(@RequestBody CuentaCliente p) {
        return cuentaService.add(p);
    }

    @GetMapping(path = {"/{id}"})
    public List<CuentaCliente> listarId(@PathVariable("id") int id) {
        return cuentaService.listarId(id);
    }

    @PutMapping(path = {"/{id}"})
    public CuentaCliente editar(@RequestBody CuentaCliente p, @PathVariable("id") int id) {
        p.setId(id);
        return cuentaService.edit(p);
    }

    @DeleteMapping(path = {"/{id}"})
    public CuentaCliente delete(@PathVariable("id") int id) {
        return cuentaService.delete(id);
    }

    @GetMapping(path = {"/movimiento/{id}"})
    public CuentaMovimientos listarMovById(@PathVariable("id") int id) {
        return movService.listarId(id);
    }

    @PostMapping("/save")
    public CuentaMovimientos agregarMovmiento(@RequestBody CuentaMovimientos mv) {
        return movService.add(mv);
    }
}
