package ni.com.ApiBanco.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

@Entity
@Table(name = "personas")
public class Persona implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column
    private String name;
    @Column
    private String apellidos;
    @Column
    private String direccion;
    @Column
    private String identificacion;

    @OneToMany(mappedBy = "persona",fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    private List<CuentaCliente> lstCuentaCliente;
            
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public List<CuentaCliente> getLstCuentaCliente() {
        return lstCuentaCliente;
    }

    public void setLstCuentaCliente(List<CuentaCliente> lstCuentaCliente) {
        this.lstCuentaCliente = lstCuentaCliente;
    }

}
