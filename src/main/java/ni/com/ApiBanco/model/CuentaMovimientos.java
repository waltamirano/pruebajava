/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.ApiBanco.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author wiston.altamirano
 */
@Entity
@Table(name = "movimientos")
public class CuentaMovimientos implements Serializable {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "fecha_movimiento")
    @Temporal(TemporalType.DATE)
    @JsonFormat(pattern = "dd-MM-yyyy")
    private Date fecha_movimiento;

    @Column(name = "hora_movimiento")
    @Temporal(TemporalType.TIME)
    @JsonFormat(pattern = "HH:mm:ss")
    private Date hora_movimiento;

    @Column(name = "tipo_movimiento")
    private int tipo_movimiento; // 1- deposito,2- retiro

    @Column(name = "movimiento")
    private float movimiento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cuenta")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private CuentaCliente cuenta;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFecha_movimiento() {
        return fecha_movimiento;
    }

    public void setFecha_movimiento(Date fecha_movimiento) {
        this.fecha_movimiento = fecha_movimiento;
    }

    public Date getHora_movimiento() {
        return hora_movimiento;
    }

    public void setHora_movimiento(Date hora_movimiento) {
        this.hora_movimiento = hora_movimiento;
    }

    public int getTipo_movimiento() {
        return tipo_movimiento;
    }

    public void setTipo_movimiento(int tipo_movimiento) {
        this.tipo_movimiento = tipo_movimiento;
    }

    public float getMovimiento() {
        return movimiento;
    }

    public void setMovimiento(float movimiento) {
        this.movimiento = movimiento;
    }

    public CuentaCliente getCuenta() {
        return cuenta;
    }

    public void setCuenta(CuentaCliente cuenta) {
        this.cuenta = cuenta;
    }

}
