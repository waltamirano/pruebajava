/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.ApiBanco.service;

import java.util.List;
import ni.com.ApiBanco.model.CuentaCliente;

/**
 *
 * @author wiston.altamirano
 */
public interface CuentaService {

    List<CuentaCliente> listar();

    List<CuentaCliente> listarId(int id);

    CuentaCliente add(CuentaCliente cc);

    CuentaCliente edit(CuentaCliente cc);

    CuentaCliente delete(int id);
}
