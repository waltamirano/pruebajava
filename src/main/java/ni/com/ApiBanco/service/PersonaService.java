
package ni.com.ApiBanco.service;

import java.util.List;
import ni.com.ApiBanco.model.Persona;

public interface PersonaService {
    List<Persona>listar();
    Persona listarId(int id);
    Persona add(Persona p);
    Persona edit(Persona p);
    Persona delete(int id);
}
