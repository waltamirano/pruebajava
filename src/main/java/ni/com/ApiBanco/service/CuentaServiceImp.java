/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.ApiBanco.service;

import java.util.List;
import ni.com.ApiBanco.model.CuentaCliente;
import ni.com.ApiBanco.repository.CuentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wiston.altamirano
 */
@Service
public class CuentaServiceImp implements CuentaService {

    @Autowired
    private CuentaRepository repositorio;

    @Override
    public List<CuentaCliente> listar() {
        return repositorio.findAll();
    }

    @Override
    public List<CuentaCliente> listarId(int id) {
        return repositorio.listarById(id);
    }

    @Override
    public CuentaCliente add(CuentaCliente cc) {
        return repositorio.save(cc);
    }

    @Override
    public CuentaCliente edit(CuentaCliente cc) {
        return repositorio.save(cc);
    }

    @Override
    public CuentaCliente delete(int id) {
        CuentaCliente cc = repositorio.findById(id);
        if (cc != null) {
            repositorio.delete(cc);
        }
        return cc;
    }

}
