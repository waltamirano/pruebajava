/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.ApiBanco.service;

import java.util.List;
import ni.com.ApiBanco.model.CuentaMovimientos;

/**
 *
 * @author wiston.altamirano
 */
public interface MovimientosService {

    List<CuentaMovimientos> listar();

    CuentaMovimientos listarId(int id);

    CuentaMovimientos add(CuentaMovimientos mv);

    CuentaMovimientos edit(CuentaMovimientos mv);

    CuentaMovimientos delete(int id);
}
