/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.ApiBanco.service;

import java.util.List;
import ni.com.ApiBanco.model.CuentaMovimientos;
import ni.com.ApiBanco.repository.MovimientosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author wiston.altamirano
 */
@Service
public class MovimientosServiceImp implements MovimientosService {

    @Autowired
    private MovimientosRepository repositorio;

    @Override
    public List<CuentaMovimientos> listar() {
        return repositorio.findAll();
    }

    @Override
    public CuentaMovimientos listarId(int id) {
        return repositorio.findById(id);
    }

    @Override
    public CuentaMovimientos add(CuentaMovimientos mv) {
        return repositorio.save(mv);
    }

    @Override
    public CuentaMovimientos edit(CuentaMovimientos mv) {
        return repositorio.save(mv);
    }

    @Override
    public CuentaMovimientos delete(int id) {
        CuentaMovimientos mv = repositorio.findById(id);
        if (mv != null) {
            repositorio.delete(mv);
        }
        return mv;
    }

}
