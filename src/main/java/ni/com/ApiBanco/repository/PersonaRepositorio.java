
package ni.com.ApiBanco.repository;


import java.util.List;
import ni.com.ApiBanco.model.Persona;
import org.springframework.data.repository.Repository;

public interface PersonaRepositorio extends Repository<Persona, Integer>{
    List<Persona>findAll();
    Persona findById(int id);
    Persona save(Persona p);
    void delete(Persona p);
}
