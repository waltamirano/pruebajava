/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.ApiBanco.repository;

import java.util.List;
import ni.com.ApiBanco.model.CuentaCliente;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

/**
 *
 * @author wiston.altamirano
 */
public interface CuentaRepository extends Repository<CuentaCliente, Integer>{
    
    List<CuentaCliente>findAll();
    
    @Query(nativeQuery = false,value = "select cc from CuentaCliente cc join fetch cc.persona p where p.id=?1")
    List<CuentaCliente> listarById(int id);
    
    CuentaCliente findById(int id);
    
    CuentaCliente save(CuentaCliente cc);
    void delete(CuentaCliente cc);
}
