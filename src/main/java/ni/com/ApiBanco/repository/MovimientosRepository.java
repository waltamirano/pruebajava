/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.com.ApiBanco.repository;

import java.util.List;
import ni.com.ApiBanco.model.CuentaMovimientos;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

/**
 *
 * @author wiston.altamirano
 */
public interface MovimientosRepository extends Repository<CuentaMovimientos, Integer> {

    List<CuentaMovimientos> findAll();

    @Query(nativeQuery = false,value = "select mv from CuentaMovimientos mv join fetch mv.cuenta c where c.id=?1")
    CuentaMovimientos findById(int id);

    CuentaMovimientos save(CuentaMovimientos mc);

    void delete(CuentaMovimientos mc);
}
